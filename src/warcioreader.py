import os
from multiprocessing.pool import ThreadPool
import time
import argparse
from warcio import ArchiveIterator

SPLITS = 5000

def main():
    """
    Given a WARC file, print all valid user defined URLs in the WARC file.
    """
    
    # Build commandline args
    parser = argparse.ArgumentParser(
                    prog='warcioreader.py',
                    description='Get user input URLs from a WARC file.',
                    epilog='Author: Chenjeff4840\t\tLast modified: 3/25/2023')
    
    parser.add_argument('-w', '--warc', required=True, help="WARC file to analyze", dest='warc')
    parser.add_argument('-v', '--verbose', help="Display statistical data, outputs url only by default",\
        action='store_true', dest='verbose')

    # Entrance pathway depending on parameters
    args = vars(parser.parse_args())
    
    # Get path
    warc_path = args["warc"]
    assert(os.path.exists(warc_path))
    
    start = time.time()
    
    # Get urls
    urls = set(get_url_list(warc_path=warc_path, verbose=args["verbose"]))
    
    # Print all urls
    for url in urls:
        print(url)
    
    if args["verbose"]:
        print("Runtime to process URLs only: {}s".format(time.time() - start))
        print("Number of URLS: {num}".format(num=len(urls)))
    
def get_url_from_records(records:list)->list[str]:
    """
    From a list of WARC records, get all user input URLs
    Param:
        records: list of WARC records
    Return: List of user input urls derived from records. Returns a 0 length
            list when no user input urls are found.
    """
    urls = []
    for record in records:
        if record.rec_type == "request" and (record.http_headers.get_header("Sec-Fetch-Site") == "same-site" or record.http_headers.get_header("Sec-Fetch-Site") == "none"):
            urls.append(record.rec_headers.get_header("WARC-Target-URI"))
    return urls
    
def get_url_list(warc_path:str, verbose:bool)->list[str]:
    """
    Grabs a list of user defined URLs at the specified path

    Args:
        warc_path (str): full file path of a WARC file
        verbose (bool): Display optional output
    Pre: warc_path exists and points to a valid WARC file.
    Returns: List of user defined urls. Returns [] if no urls exist
    """
    urls = []
    records = []
    with open(warc_path, 'rb') as stream:
        # TODO make more efficient
        # Get a list of records
        records = [e for e in ArchiveIterator(stream)]
    if verbose:
        print("Number of records found: {}".format(len(records)))
    # Split the list s.t. the lists are grouped in <= 1000 records
    # Generate pool
    pool = ThreadPool(processes=int(len(records) / SPLITS) if len(records) > SPLITS else 1)
    async_tasks = []
    
    # Run tasks
    for i in range(0, len(records), SPLITS):
        async_tasks.append(pool.apply_async(get_url_from_records, (records[i:i+SPLITS if len(records) >= i + SPLITS else len(records) + 1],)))
    
    # Wait for tasks and cat lists
    for task in async_tasks:
        urls += task.get()
    return urls

if __name__ == "__main__":
    main()

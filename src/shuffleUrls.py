import random
from urllib.parse import urlparse


def shuffle_urls(url_list):
    random.shuffle(url_list)
    return url_list


def add_url(url_list, new_url):
    # Parse the new URL to extract its domain.
    new_url_parsed = urlparse(new_url)
    new_domain = new_url_parsed.netloc

    # Initialize the minimum distance between same-domain URLs and the index at which to insert the new URL.
    min_distance = len(url_list) + 1
    insert_index = -1

    # Iterate through the URL list and find the optimal position for the new URL.
    for i, url in enumerate(url_list):
        # Parse the current URL to extract its domain.
        url_parsed = urlparse(url)
        domain = url_parsed.netloc

        # Check if the current URL's domain matches the new URL's domain.
        if domain == new_domain:
            # Calculate the distance between the current URL and the last occurrence of the same domain.
            distance = i - insert_index

            # If the calculated distance is smaller than the current minimum distance,
            # update the minimum distance and the index at which to insert the new URL.
            if distance < min_distance:
                min_distance = distance
                insert_index = i

    # If the optimal position is not found, append the new URL to the end of the list.
    if insert_index == -1:
        url_list.append(new_url)
    # Otherwise, insert the new URL at the optimal position.
    else:
        url_list.insert(insert_index + 1, new_url)

    # Return the updated list of URLs.
    return url_list

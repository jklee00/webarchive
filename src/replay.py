import subprocess
import os
import webbrowser
import signal
import time
import argparse

def main():
    """
    Program runner to replay a single WARC file
    """
    
    # Change directory to make sure wb-manager always saves collections to specified directory 
    root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    os.chdir(root)
    
    # Build commandline args
    parser = argparse.ArgumentParser(
                    prog='replay.py',
                    description='pywb replayer. Replays a stored WARC file or replay previously\
                        archived collections through switches.',
                    epilog='Author: Chenjeff4840\t\tLast modified: 3/21/2023')
    
    parser.add_argument('-a', '--archived', help="Replay previously archived files", action='store_true', dest='archived')
    
    # Entrance pathway depending on parameters
    args = vars(parser.parse_args())
    
    if args['archived']:
        replay_collection()
    else:
        # User input WARC path
        warc_path = input("Input a full path of a WARC file> ").replace("\"", "")
        assert(os.path.exists(warc_path))
        
        # Generate the collection
        collection_name = os.path.basename(warc_path).rpartition('.')[0]
        subprocess.run(["wb-manager", "init", collection_name])

            
        # Add the warc file to the created collection
        subprocess.run(["wb-manager", "add", collection_name, warc_path], shell=False)
        
        # Replay the collection
        replay_collection()
    
def replay_collection()->None:
    """
    Replays a collection

    Args:
        collection (str): A collection to replay
    """    
    # Run wayback
    replayer = subprocess.Popen(["wayback", "-a"])
    time.sleep(2)
    
    # Open web browser
    webbrowser.open_new_tab("http://localhost:8080/")
    
    # Wait for user to signal they want to quit
    user_quit_input = None
    while user_quit_input != "quit()":
        user_quit_input = input("\nEnter quit() to terminate\n")
    
    # Kill wayback
    os.kill(replayer.pid, signal.CTRL_C_EVENT)
    
if __name__ == "__main__":
        main()

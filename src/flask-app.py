import subprocess
from flask import Flask
from flask import Response, request
import os
from flask import jsonify
from psutil import process_iter
from signal import SIGTERM


app = Flask(__name__)

# Default route to check if you are running it correctly
@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

##########################################################
# Route used to record URLS. Request body in format:
####################################
# Request body:
#{
#   "urls":[list of urls]    
#}
####################################
# Reponse body: EMPTY
@app.route("/record", methods=['POST'])
def record_urls():
    # Kill server if it exists
    kill_process_on_port(8080)
    
    # Check that urls field exists
    if "urls" not in request.json:
        return Response("ERROR: Request Body does not contain \"urls\" field")
    
    # Get urls from body
    urls = request.json["urls"]
    print('Recieved from client: {}'.format(urls))
    
    # Call record.py
    return Response('SUCCESS: WARNING Bulk URL recording is not supported as this moment')

##########################################################
# Route used to replay already archived files
####################################
# Request body: EMPTY
####################################
# Response body: EMPTY
@app.route("/replay/archived", methods=['GET'])
def replay_archived():
    # Kill server if it exists
    kill_process_on_port(8080)
    
    # Change directory to meet preconditions of replay
    root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    os.chdir(root)
    # Replay collections
    subprocess.Popen(["python", os.path.join(root, "src", "replay.py"), "-a"])

    return Response('SUCCESS: archival browser launched.')

##########################################################
# Route used to get user input URLs.
####################################
# Request body:
#{
#   "warc":<WARC file path>    
#}
##################################### 
# Response body
#{
#   "urls":[list of urls]    
#}
@app.route("/replay/urls", methods=['GET'])
def replay_archived_urls():
    # Replay collections
    if "warc" not in request.json:
        return Response("ERROR: Request Body does not contain \"warc\" field")
    
    # Call function
    root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    result = subprocess.run(["python", os.path.join(root, "src", "warcioreader.py"), "-w", request.json["warc"]],\
        stdout=subprocess.PIPE, text=True)
    
    # Package output into JSON
    urls = result.stdout.split("\n")
    
    # -1 to get rid of new line
    resp = {"urls":urls[:len(urls) - 1]}
    
    return jsonify(resp)

def kill_process_on_port(port:int)->None:
    """
    Kills a process bound to a port

    Args:
        port (int): port of process to kill
    """
    
    for proc in process_iter():
        for conns in proc.connections(kind='inet'):
            if conns.laddr.port == port:
                proc.send_signal(SIGTERM) # or SIGKILL
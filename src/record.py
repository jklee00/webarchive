import subprocess
import os
import webbrowser
import time
import replay
import pyautogui
from selenium import webdriver
import keyboard

def main():
    #root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    url_list = "src/" + input("Input a file with a list of urls to record> ")
    collection_name = input("Input a collection name> ")

    os.chdir("../")

    subprocess.run(["wb-manager", "init", collection_name])
    # subprocess.run(["wb-manager", "add", collection_name])
    time.sleep(4)

    wayback = subprocess.Popen(["wayback", "--record", "--live", "-a", "--auto-interval", "10"])

    print("\n\nAfter a few seconds, recording will automatically halt")
    time.sleep(2)
    with open(url_list) as file:
        for line in file:
            webbrowser.open_new_tab("http://localhost:8080/" + collection_name + "/record/" + line.strip() + "/")
            time.sleep(10)
            pyautogui.hotkey('cmd', 'w')
            pyautogui.hotkey('ctrl', 'w')

    wayback.kill()
    # replay.replay_collection()

if __name__ == "__main__":
    main()
    replay.replay_collection()
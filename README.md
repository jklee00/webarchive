# Flask Server Setup
1. Goto the src folder
2. Install Venv - WINDOWS: "py -3 -m venv venv", LINUX "python3 -m venv venv"
3. Activate Venv - WINDOWS: "venv/Scripts/activate", LINUX ".venv/bin/activate"
4. "pip install Flask"
5. "pip install psutil"

The reason venv is needed is because Flask and pywb rely on 2 different distributions of several packages with pywb unsurprisingly being versions behind on some. Venv will create a virtual enviroment which hosts Flask packages while the flask app will run replay.py and other py files using your non-virtual python installation.

# Running Flask Server
1. Always activate Venv before executing next command.
2. "flask --app flask-app  run"
3. You can explore server with "localhost:5000"
4. View flask-app.py file for routes that can be used. For example, "localhost:5000/replay/archived" replays all saved archives.
